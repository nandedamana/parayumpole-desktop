/*    This file is part of the desktop version of
 *    Parayumpole bi-directional transliteration tool for Indian languages v0.4.2,
 *    and is also distributed with the Web version of Parayumpole.
 *    
 *    libparayumpole.js
 *    v3.0
 *    
 *    Copyright (C) 2013-2018 Nandakumar Edamana <nandakumar@nandakumar.co.in>
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
 
/*
 * v3.0 21 Apr 2018:
 *   New series to indicate the rise of libparayumpole.js as a common resource for
 *   the Web and desktop versions.
 * v2.1.1.2 20 Apr 2018:
 *   Bugfix: Prevent ntha from being ന്റ്ഹ instead of ന്ത
 * v2.1.1.1 25 Jul 2017:
 *   Bugfixes (mainly undefined chillu issues in languages other than ml)
 * v2.1.1 25 Jul 2017:
 *   Bugfixes
 * Modified on 21 Jun 2017: Strted adding languages other than Malayalam (copied from my other works [2013] )
 */

/* TODO introduce SCRIPT_EN and use them instead of src-dest arrays scheme WHAT DOES THIS MEAN? I couldn't understand this on 21 Apr 2018. */
/* TODO standardize quotes (single or double) */
/* TODO FIXME varnames: use 'input', 'output' instead of 'en' and 'ml' */

const SCRIPT_EN = 0;
const SCRIPT_ML = 1;
const SCRIPT_TA = 2;
const SCRIPT_KN = 3;
const SCRIPT_TE = 4;
const SCRIPT_HI = 5;

const LANGCODES = {'en': SCRIPT_EN, 'ml': SCRIPT_ML, 'ta': SCRIPT_TA, 'kn': SCRIPT_KN, 'te': SCRIPT_TE, 'hi': SCRIPT_HI};

const VERBATIM = [
	' ', '\n', '\t',
	'.', ',', '-', '_', '?', '&', '!', '+', '=', '*', '%', '(', ')', /* TODO FIXME arrange, TODO quotes */
	';', /* TODO FIXME : is used for visargam now */
	'\'', '"',
	'1', '2', '3', '4', '5', '6', '7', '8', '9', '0'
]; /* TODO add more and remove duplicates from the array cons */

const VOW_PRV_CHAR = VERBATIM.concat([undefined, '|', '{']); /* Characters that indicate the next vowel should not be rendered a symbol. prv_char will be undefined at the start of the whole text. */

const replace2_src = ["aa","ee","oo","ai","ou"];
const replace2_new = ["A","^","U","I","<"];

const vow=[];
vow[SCRIPT_EN]=['a', 'A', 'i', '^', 'u', 'U', '~', 'e', 'E', 'I', 'o', 'O', '<'];
vow[SCRIPT_ML]=['അ', 'ആ', 'ഇ', 'ഈ', 'ഉ', 'ഊ', 'ഋ', 'എ', 'ഏ', 'ഐ', 'ഒ', 'ഓ', 'ഔ'];
vow[SCRIPT_TA]=['அ', 'ஆ', 'இ', 'ஈ', 'உ', 'ஊ', '௲', 'எ', 'ஏ', 'ஐ', 'ஒ', 'ஓ', 'ஔ'];
vow[SCRIPT_KN]=['ಅ', 'ಆ', 'ಇ', 'ಈ', 'ಉ', 'ಊ', 'ಋ', 'ಎ', 'ಏ', 'ಐ', 'ಒ', 'ಓ', 'ಔ'];
vow[SCRIPT_TE]=['అ', 'ఆ', 'ఇ', 'ఈ', 'ఉ', 'ఊ', 'ఋ', 'ఎ', 'ఏ', 'ఐ', 'ఒ', 'ఓ', 'ఔ']; /* TODO FIXME */
vow[SCRIPT_HI]=['अ', 'आ', 'इ', 'इ', 'उ', 'उ', 'ृ', 'ए', 'ए', 'ऐ', 'ओ', 'ओ', 'औ']; /* TODO FIXME */

/* TODO integrate vowels and symbols */
const symbols=[]
//symbols=["a","A","i","^","u","U","~","e","E","I","o","O","<","`","\\"];
symbols[SCRIPT_ML]=["","ാ","ി","ീ","ു","ൂ","ൃ","െ","േ","ൈ","ൊ","ോ","ൗ","ം","്‍"];
symbols[SCRIPT_TA]=["","ா","ி","ீ","ு","ூ","௲","ெ","ே","ை","ொ","ோ","ௌ","ஂ","ஞ"];
symbols[SCRIPT_KN]=["","ಾ","ಿ","ೀ","ು","ೂ","ೃ","ೆ","ೇ","ೌ","ೊ","ೋ","ೌ","ಂ","ಃ"];
/* TODO TE */
symbols[SCRIPT_HI]=["","ा","ि","ी","ु","ू","ृ","े","े","ै","ो","ो","ौ","ं","ः"]; /* TODO FIXME */

const ZWNJ=[]
ZWNJ[SCRIPT_ML]='‌'; /* There is; invisible */
ZWNJ[SCRIPT_TA]=''; /* No ZWNJ? */
ZWNJ[SCRIPT_KN]=''; /* TODO FIXME No ZWNJ? */
/* TODO TE */
/* TODO HI */

const CHANDRAKKALA=[]
CHANDRAKKALA[SCRIPT_ML]='്';
CHANDRAKKALA[SCRIPT_TA]='்';
CHANDRAKKALA[SCRIPT_KN]='್';
/* TODO TE */
CHANDRAKKALA[SCRIPT_HI]='्';

const ANUSWARAM = [];
ANUSWARAM[SCRIPT_ML] = "ം";
ANUSWARAM[SCRIPT_TA] = "ம்";
ANUSWARAM[SCRIPT_KN] = "ಂ";
/* TODO TE */
ANUSWARAM[SCRIPT_HI] = "ं";

/* TODO FIXME create an array VERBATIM and move many chars from these to it */
const cons=[];
cons[SCRIPT_EN]=["k","K","g","G","c","C","j","J","t","T","d","D","N","n","p","P","b","B","m","y","r","l","v","S","s","h","L","Z","R","f","M","/", ':'];
cons[SCRIPT_ML]=["ക","ഖ","ഗ","ഘ","ച","ഛ","ജ","ഝ","ട","ഠ","ദ","ഡ","ണ","ന","പ","ഫ","ബ","ഭ","മ","യ","ര","ല","വ","ശ","സ","ഹ","ള","ഴ","റ","ഫ","മ്മ","ഽ","ഃ"];
cons[SCRIPT_TA]=["க","க","க","க","ச","ச","ஜ","ஜ","ட","ட","த","ட","ண","ந","ப","ப","ப","ப","ம","ய","ர","ல","வ","ஶ","ஸ","ஹ","ள","ழ","ற","ப","ம்ம", '?',"ஃ"]; /* TODO FIXME last one */
cons[SCRIPT_KN]=["ಕ","ಖ","ಗ","ಘ","ಚ","ಛ","ಜ","ಝ","ಟ","ಠ","ದ","ಡ","ಣ","ನ","ಪ","ಫ","ಬ","ಭ","ಮ","ಯ","ರ","ಲ","ವ","ಶ","ಸ","ಹ","ಳ","?","ಱ","ಫ","ಮ್ಮ",'?', "ഃ"]; /* TODO FIXME last one */
cons[SCRIPT_HI]=["क","ख","ग","घ","च","छ","ज","झ","ट","ठ","द","ड","ण","न","प","फ","ब","भ","म","य","र","ल","व","श","स","ह",'?',"?","ಱ","ಫ","ಮ್ಮ",'?', "ഃ"]; /* TODO FIXME last one */

const hchar=[];
hchar[SCRIPT_EN]=["k","g","c","j","n","t","T","d","N","D","p","b","s","z"];
hchar[SCRIPT_ML]=["ഖ","ഘ","ച","ഝ","ഞ","ത","ഥ","ധ","ന","ഢ","ഫ","ഭ","ഷ","ഴ"];
hchar[SCRIPT_TA]=['க','க','ச','ச','ஞ','த','த','த','ன','ட','ப','ப','ஷ','ழ'];
hchar[SCRIPT_KN]=["ಖ","ಘ","ಚ","ಝ","ಞ","ತ","ಥ","ಧ","N","ಢ","ಫ","ಭ","ಷ","?"]; /* TODO FIXME */
hchar[SCRIPT_HI]=["ख","घ","च","छ","ഞ","त","थ","ध","न","ढ","फ","भ","ष","ष"]; /* TODO FIXME */
/* TODO FIXME what about Tamil? */

const special_char1 = ['n', 'n', 'n', 'm', 'n', 'N', 'l', 'L', 'r'];
const special_char2 = ['k', 'g', 't', 'p', '\\', '\\', '\\', '\\', '\\'];
const special_char_next_is_not = { 'nt': 'h' } // To prevent ntha from being ന്റ്ഹ instead of ന്ത
const special_replace = [];
/* Chandrakkala is the default if no rule is specified. */
const special_replace_can_have_chandrakkala = [undefined, undefined, undefined, undefined, undefined];
special_replace[SCRIPT_ML] = ["ങ്ക", "ങ", 'ന്റ', 'മ്പ', 'ന്‍', 'ണ്‍', 'ല്‍', 'ള്‍', 'ര്‍'];
special_replace_can_have_chandrakkala[SCRIPT_ML] = [true, true, true, true, false, false, false, false, false];
special_replace[SCRIPT_TA] = [undefined, "ங", 'nt', undefined, 'ன', undefined, undefined, undefined, undefined]; /* TODO FIXME */
special_replace[SCRIPT_KN] = [undefined, "ಙ", 'nt', undefined, undefined, undefined, undefined, undefined, undefined];

function nan_transliterate(input, script_out_name) {
	input = new String(input); // TODO FIXME more clean way? This is not needed in Browser but in gjs
	var script_out = LANGCODES[script_out_name];

	var ml = ""; /* TODO rename */

	/* First pass */
	/* Cannot use string replacement methods since enable/disble special chars ({}) can
	 * be present inside. TODO But, is there a better way to do this?
	 */
	var input_replaced = '';
	var pos = 0;
	while(pos < input.length) {
		if(input[pos] == '{') {
			endPos = input.indexOf('}', pos + 1);
/* TODO FIXME add feature for escaping } like \} */
			if(endPos >= 0) {
				input_replaced += input.substr(pos, endPos + 1); /* endPos now means length */
				pos += endPos + 2;
				continue;
			}
			else {
	
if(NAN_DEBUG_MODE) // TODO
	ues.write("TODO FIXME: something not processed?", null); /* TODO FIXME error */
			}
		}

		var ch_new = input[pos];

		var replaceIndex;
		if( (replaceIndex = replace2_src.indexOf(input.substr(pos, 2))) >= 0) {
			ch_new = replace2_new[replaceIndex];
			pos ++;
		}

		input_replaced += ch_new;
		pos ++;
	}

	input = input_replaced;

	/* Second pass */
	pos = 0;
	var disabled = false;

	mainTransLoop:
	while(pos < input.length) {
		var prv_ch = undefined, nxt_ch = undefined, later_ch = undefined;
		var ch = input[pos];

		if(ch == '{') {
			disabled = true;
			pos ++;
			continue mainTransLoop;
		}
		else if(ch == "}") {
			disabled = false;
			pos ++;
			continue mainTransLoop;
		}

		if(disabled || VERBATIM.indexOf(ch) >= 0 ) { /* {english} or VERBATIM char */
			ml += ch;
			pos ++;
			continue mainTransLoop;
		}
		else {
			if(pos > 0)                  { prv_ch = input[pos - 1]; }
			if(pos < (input.length - 1)) { nxt_ch = input[pos + 1]; }
			if(pos < (input.length - 2)) { later_ch = input[pos + 2]; }

			var vowIndex;
			if( (vowIndex = vow[SCRIPT_EN].indexOf(ch)) >= 0) { /* Is a vowel or a symbol */
				if(VOW_PRV_CHAR.indexOf(prv_ch) >= 0) { /* Not a symbol */
					ml += vow[script_out][vowIndex];
				}
				else { /* Is a sumbol */
					ml += symbols[script_out][vowIndex]
				}

				pos ++;
				continue mainTransLoop;
			}

			/* Some special cases like nj and ng */
			if(typeof(special_replace[script_out]) != 'undefined' ) {
				for(var i in special_char1) {
					if( ch == special_char1[i] &&
						nxt_ch == special_char2[i] &&
						( special_char_next_is_not[ch + nxt_ch] == undefined ||
								special_char_next_is_not[ch + nxt_ch] != later_ch ) &&
						typeof(special_replace[script_out][i]) != 'undefined' ) {
						
						ml += special_replace[script_out][i];
						if(vow[SCRIPT_EN].indexOf(later_ch) < 0) {
							/* Either no rule is specified or the specified rule allows Chandrakkala in this case. */
							if(typeof(special_replace_can_have_chandrakkala[script_out]) == 'undefined' ||
								 special_replace_can_have_chandrakkala[script_out][i] == true
							) {
								ml += CHANDRAKKALA[script_out];
							}
						}

						pos += 2;
						continue mainTransLoop;
					}
				}
			}

			/* Anuswaram */
			if(ch == 'm' && nxt_ch != 'm' && VOW_PRV_CHAR.indexOf(prv_ch) < 0 && vow[SCRIPT_EN].indexOf(nxt_ch) < 0) {
				ml += ANUSWARAM[script_out];
				pos ++;
				continue mainTransLoop;
			}

			/* Special chars like kh, gh, etc. */
			if(nxt_ch == 'h') {
				for(i in hchar[SCRIPT_EN]) {
					if(hchar[SCRIPT_EN][i] == ch) {
/* TODO FIXME */
if(NAN_DEBUG_MODE)
	ues.write("considering ch="+ch+" and hchar[SCRIPT_EN][i]=" +hchar[SCRIPT_EN][i], null);
						ml += hchar[script_out][i];
						if(vow[SCRIPT_EN].indexOf(later_ch) < 0) {
							ml += CHANDRAKKALA[script_out];
						}

						pos += 2;
						continue mainTransLoop;
					}
				}
			}
			
			/* Rest of the consonants */
			var consIndex;
			if( (consIndex = cons[SCRIPT_EN].indexOf(ch)) >= 0) {
				ml += cons[script_out][consIndex];
				if(vow[SCRIPT_EN].indexOf(nxt_ch) < 0) {
					ml += CHANDRAKKALA[script_out];
				}
			}
			else {
/* TODO FIXME */
if(NAN_DEBUG_MODE)
	ues.write("TODO FIXME: something not processed? ch="+ch, null);
			}

			pos ++;
		}
	}

	return ml;
};

/* Nandakumar Edamana
 * 21 Jun 2017
 */

/* TODO FIXME standardize quotes (single or double) */

var script_src = SCRIPT_ML;

const vow_rev = [];
vow_rev[SCRIPT_EN] = ["a","A","i","ee","u","U","~","e","E","I","o","O","ou"];
vow_rev[SCRIPT_ML] = ["അ","ആ","ഇ","ഈ","ഉ","ഊ","ഋ","എ","ഏ","ഐ","ഒ","ഓ","ഔ"];
vow_rev[SCRIPT_TA] = ["அ","ஆ","இ","ஈ","உ","ஊ","௲","எ","ஏ","ஐ","ஒ","ஓ","ஔ"];
vow_rev[SCRIPT_KN] = ["ಅ","ಆ","ಇ","ಈ","ಉ","ಊ","ಋ","ಎ","ಏ","ಐ","ಒ","ಓ","ಔ"];

/* TODO FIXME move many of these to verbatim */
/* TODO FIXME two types of 'na' issues */
const cons_rev=[];
cons_rev[SCRIPT_EN]=[
"k","K","g","G","ng",
"c","C","j","J","nj",
"t","T","d","D","N",
"th","Th","d","dh","n","Nh",
"p","P","b","B","m",
"y","r","l","v","S","sh","s","h","L","Z","R","f","M","'","\"",";",":","?","!","/","-","_","+","=","(",")","&","%","@","$","1","2","3","4","5","6","7","8","9","0"];
cons_rev[SCRIPT_ML] = [
"ക","ഖ","ഗ","ഘ","ങ",
"ച","ഛ","ജ","ഝ","ഞ",
"ട","ഠ","ദ","ഡ","ണ",
"ത","ഥ","ദ","ധ","ന","ന",
"പ","ഫ","ബ","ഭ","മ",
"യ","ര","ല","വ","ശ","ഷ","സ","ഹ","ള","ഴ","റ","ഫ","മ്മ","'","\"",";",":","?","!","ഽ","-","_","+","=","(",")","&","%","@","ഃ","൧","൨","൩","൪","൫","൬","൭","൮","൯","൦"]; 
/* TODO FIXME there is no need of this much duplication in Tamil */
cons_rev[SCRIPT_TA] = [
"க","க","க","க","ங",
"ச","ச","ஜ","ஜ","ஞ",
"ட","ட","ட","ட","ண",
"த","த","த","த","ந","ன",
"ப","ப","ப","ப","ம",
"ய","ர","ல","வ","ஶ","ஷ","ஸ","ஹ","ள","ழ","ற","ப","ம்ம","'","\"",";",":","?","!","/","-","_","+","=","(",")","&","%","@","ஃ","1","2","3","4","5","6","7","8","9","0"];
cons_rev[SCRIPT_KN] = [
"ಕ","ಖ","ಗ","ಘ","ಙ",
"ಚ","ಛ","ಜ","ಝ","ಞ",
"ಟ","ಠ","ದ","ಡ","ಣ",
"ತ","ಥ","ದ","ಧ","ನ","ನ",
"ಪ","ಫ","ಬ","ಭ","ಮ","ಯ",
"ರ","ಲ","ವ","ಶ","ಷ","ಸ","ಹ","ಳ","?","ಱ","ಫ","ಮ್ಮ","'","\"",";",":","?","!","/","-","_","+","=","(",")","&","%","@","ഃ","൧","൨","൩","൪","൫","൬","൭","൮","൯","൦"];

/* TODO FIXME: EA -> O ? (േ + ാ = ോ) */

const sym_rev = [];
sym_rev[SCRIPT_EN] = ["","A","i","ee","u","U","~","e","E","I","o","O","ou","m","\\"];
sym_rev[SCRIPT_ML] = ["്","ാ","ി","ീ","ു","ൂ","ൃ","െ","േ","ൈ","ൊ","ോ","ൗ","ം","‍"]; /* The last one is for chillu */
sym_rev[SCRIPT_TA] = ["்","ா","ி","ீ","ு","ூ","௲","ெ","ே","ை","ொ","ோ","ௌ","ஂ","?"]; /* TODO FIXME
 tamil chillu mismatch issue */
sym_rev[SCRIPT_KN] = ["್","ಾ","ಿ","ೀ","ು","ೂ","ೃ","ೆ","ೇ","ೈ","ೊ","ೋ","ೌ","ಂ","ಃ"];

/* TODO FIXME */
var src_specific_rev = [];
var dest_specific_rev = [];

function nan_transliterate_reverse(input, script_src_name) {
	script_src = LANGCODES[script_src_name];

	// Cannot concat while coding. We need the arrays in original form also.
	var src = [].concat(vow_rev[script_src], sym_rev[script_src], cons_rev[script_src]); /* src chars */
	var dest = [].concat(vow_rev[SCRIPT_EN], sym_rev[SCRIPT_EN], cons_rev[SCRIPT_EN]);

	var output = "";
	var pos = 0;

	var verbatim = false; /* Copy input text inside {} */
	while(pos < input.length) {
		var prv_ch ="", next_ch="", later_ch=""; /* TODO FIXME use undefined instead */
		var ch = input[pos];

		/* TODO FIXME is this the correct methods? */
		if(dest.indexOf(ch) >= 0) { /* ch is present in the destination character set */
			if(!verbatim) {
				output += '{';
				verbatim = true;
			}

			output += ch;
			pos ++;
			continue;
		}
		/* VERBATIM.indexOf(ch) is given here since its absence cause each space to close and reopen {} */
		else if(verbatim && VERBATIM.indexOf(ch) < 0) { /* Not a dest nor verbatim char; but verbatim mode is active => close {} */
			output += '}';
			verbatim = false;
		}

		if(pos > 0) {
			prv_ch = input[pos - 1]; // TODO rem
		}

		if(pos < (input.length - 1)) {
			next_ch = input[pos + 1];
		}

		if(pos < (input.length - 2)) {
			later_ch = input[pos + 2]; // TODO rem
		}

		if(VERBATIM.indexOf(ch) >= 0) { /* ch is a verbatim char. */
			output += ch;
		}
		else {
			for(var i in src) {
				if(src[i] == ch) {
					if(typeof(ANUSWARAM[script_src]) != 'undefined' &&
					 ch == ANUSWARAM[script_src] &&
					 sym_rev[script_src].indexOf(prv_ch) < 0
					) { /* Only 'am' will be handled by this if. 'im', 'um', etc. will be automatically handled since there is no need of preppending 'a'. */
						output += 'am';
						break;
					}

					output += dest[i];

					if(sym_rev[script_src].indexOf(ch) < 0 &&
					   sym_rev[script_src].indexOf(next_ch) < 0 &&
					   vow_rev[script_src].indexOf(ch) < 0
					) {
						output += 'a';
					}
					break;
				}
			}
		}

		pos++;
	}

	return output;
};

if(typeof(NAN_DEBUG_MODE) == 'undefined')
	var NAN_DEBUG_MODE = false; // XXX
