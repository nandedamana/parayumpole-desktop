#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#       This file is part of the desktop version of
#       Parayumpole bi-directional transliteration tool for Indian languages v0.4.2
#
#       parayumpole.py
#       v0.4.2
#       
#       Copyright (C) 2012, 2013, 2017, 2018, 2022 Nandakumar Edamana <nandakumar@nandakumar.co.in>
#       
#       This program is free software; you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation; version 3.
#       
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#       
#       You should have received a copy of the GNU General Public License
#       along with this program; if not, write to the Free Software
#       Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#       MA 02110-1301, USA.
#       
#       

from collections import OrderedDict
import os, sys
import subprocess
import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
from gi.repository import Gdk
import webbrowser

ONLINE_HELP_PAGE = 'https://nandakumar.co.in/software/parayumpole/'
VERSION = '0.4.2'
NAN_LANG_TITLES = OrderedDict([('ml', 'Malayalam'),
	('ta', 'Tamil'),
	('kn', 'Kannada'),
	('hi', 'Hindi'),
	('en', 'English')])

# TODO use gettext
def _(txt):
	return txt

class NanParayumpoleBackend:
	gjs = None

	def __init__(self):
		libdir = '/usr/lib/parayumpole'
		if len(sys.argv) == 3 and sys.argv[1] == '--libdir': # TODO systematic approach
			libdir = sys.argv[2]

		# Throws exception
		self.gjs = subprocess.Popen(['gjs', '-I', libdir, libdir + '/backend.js'], bufsize = 1, encoding = 'utf-8', stdin = subprocess.PIPE, stdout = subprocess.PIPE)

		self.inlang = 'en'
		self.outlang = 'ml'
		self.lines_in_prv = [] # To check which lines to be newly transliterated TODO make use of
		
	def __del__(self):
		if self.gjs:
			print("Killing gjs...") # TODO REM
			self.gjs.kill()
		
	def convert(self, text):
		if text == '' or self.inlang == self.outlang:
			return text
		
		# This splitting not only helps avoid unnecessary re-processing,
		# but also avoids the newline confusions in pipes.
		# TODO make use of self.lines_in_prv and avoid unnecessary re-processing
		lines = text.split('\n')
		out = ''
		for line in lines:
			try: # TODO more scientific way?
				self.gjs.stdin.write(self.inlang + '\n' + self.outlang + '\n' + line + '\n')
				self.gjs.stdin.flush()
				#print('py writing: '+line) # TODO REM
				#print('py waiting...') # TODO REM
				conv = self.gjs.stdout.readline()
				out += conv
				#print('py read:' + conv) # TODO REM
			except Exception as e:
				print(e) # TODO stderr
				return None
		return out

	def set_inlang(self, lang):
		self.inlang = lang

	def set_outlang(self, lang):
		self.outlang = lang

class NanTab:
	def __init__(self):
		self.title = None
		self.saved = False
		self.changed = False
		self.fname = None
		
	def set_saved(self):
		self.saved = True
		self.changed = False
		self.nanEditorPage.parayumpoleApp.notebook.set_tab_label_text(
			self.nanEditorPage.vbox, self.title)
		
	def set_changed(self):
		self.changed = True
		self.saved = False
		self.nanEditorPage.parayumpoleApp.notebook.set_tab_label_text(
			self.nanEditorPage.vbox, '*' + self.title)

class NanEditorPage:
	def __init__(self, parayumpoleApp, nanTab):
		self.parayumpoleApp = parayumpoleApp
		self.nanTab = nanTab
		self.nanTab.nanEditorPage = self
	
		self.txtview_in = Gtk.TextView()
		self.txtview_in.set_wrap_mode(Gtk.WrapMode.WORD)
		self.txtbuf_in = self.txtview_in.get_buffer()
		
		self.txtview_out = Gtk.TextView()
		self.txtview_out.set_wrap_mode(Gtk.WrapMode.WORD)
		self.txtview_out.set_editable(False)
		self.txtbuf_out = self.txtview_out.get_buffer()
		
		self.txtbuf_in.txtbuf_out = self.txtbuf_out
		self.txtbuf_in.nanTab = nanTab
		self.txtbuf_in.connect('changed', parayumpoleApp.on_txtbuf_in_changed)
		
		self.sw_in = Gtk.ScrolledWindow()
		self.sw_in.set_shadow_type(1) # TODO FIXME needed?
		self.sw_in.add(self.txtview_in)
		# Scrollbar policy
		self.sw_in.set_policy(Gtk.PolicyType.AUTOMATIC, Gtk.PolicyType.AUTOMATIC)
		
		self.sw_out = Gtk.ScrolledWindow()
		self.sw_out.set_shadow_type(1) # TODO FIXME needed?
		self.sw_out.add(self.txtview_out)
		self.sw_out.set_policy(Gtk.PolicyType.AUTOMATIC, Gtk.PolicyType.AUTOMATIC)
		
		self.vbox = Gtk.VBox()
		self.vbox.add(self.sw_in)
		self.vbox.add(self.sw_out)
		
		# For easy access while handling events after embedding in notebook pages
		self.vbox.txtbuf_in = self.txtbuf_in
		self.vbox.txtbuf_out = self.txtbuf_out

class main():
	topmenus = OrderedDict([
		('file', {'label': '_File', 'children': None, 'enabled': True}),
		('edit', {'label': '_Edit', 'children': None, 'enabled': False}),
		('tabs', {'label': '_Tabs', 'children': None, 'enabled': True}),
		('help', {'label': '_Help', 'children': None, 'enabled': True})
	])
	
	def __init__(self):
		# TODO err
		
		# TODO add tooltips for menus
		# I have used regular dictionaries ({}) inside since order is not an issue there.
		self.topmenus['file']['children'] = OrderedDict([
			('open_ip',  {'label': '_Open Input', 'callback': self.on_open_input, 'accelkey': 'o', 'accelmods': Gdk.ModifierType.CONTROL_MASK}), # accelmods specified for demonstration. Can be left undefined, in which case it will be set to CONTROL_MASK
			('save_ip',  {'label': '_Save Input', 'callback': self.on_save_input, 'accelkey': 's'}),
			('save_op',  {'label': 'Save Output', 'callback': self.on_save_output, 'accelkey': 'e'}), # e => export
			('hsep1', {'label': '-', 'callback': None}),
			('quit',  {'label': '_Quit', 'callback': self.on_quit, 'accelkey': 'q'})
		]);

		self.topmenus['edit']['children'] = OrderedDict([
			('cut',   {'label': 'Cu_t', 'callback': None}), # TODO
			('copy',  {'label': '_Copy', 'callback': None}), # TODO
			('paste', {'label': '_Paste', 'callback': None}), # TODO
			('hsep2', {'label': '-', 'callback': None}), # TODO
			('selall', {'label': 'Select _All', 'callback': None}) # TODO
		]);

		self.topmenus['tabs']['children'] = OrderedDict([
			('new', {'label': '_New Tab', 'callback': self.on_mnu_tab_new, 'accelkey': 't'})
		]);
		
		self.topmenus['help']['children'] = OrderedDict([
			('online_help', {'label': 'Online _Help', 'callback': self.on_online_help, 'accelkey': 'F1', 'accelmods': 0}),
			('about', {'label': '_About', 'callback': self.on_about})
		]);
		
		self.tabs = []
		self.tabcount_untitled = 0
		self.quit_confirm_shown = False
		
		self.create_main_window()
		self.wnd.show_all()
	
	def create_main_window(self):
		self.wnd = Gtk.Window()
		self.wnd.set_title(_('Parayumpole'))
		self.wnd.set_icon_name('parayumpole')
		self.wnd.set_size_request(600, 550)
		self.wnd.set_position(Gtk.WindowPosition.CENTER)
					
		self.wnd.connect('delete-event', self.on_quit)
		self.wnd.connect('destroy', self.on_destroy) # TODO Added in addition to the 'delete-event' handler in the hope that it will handle unexpected situations
		
		self.vbox = Gtk.VBox()
		self.mbr = Gtk.MenuBar()
		self.accelgroup = Gtk.AccelGroup()
		self.wnd.add_accel_group(self.accelgroup)

		for menuobj in self.topmenus.values():		
			menuobj['gtkMenuItem'] = Gtk.MenuItem(label=menuobj['label'])
			menuobj['gtkMenuItem'].set_sensitive(menuobj['enabled'])
			menuobj['gtkMenuItem'].set_use_underline(True)
			if menuobj['children'] != None:
				menuobj['gtkSubMenu'] = Gtk.Menu()
				menuobj['gtkMenuItem'].set_submenu(menuobj['gtkSubMenu'])
				for childobj in menuobj['children'].values():
					if childobj['label'] != '-':
						childobj['gtkMenuItem'] = Gtk.MenuItem(label=childobj['label'])
						childobj['gtkMenuItem'].set_use_underline(True)
					else:
						childobj['gtkMenuItem'] = Gtk.SeparatorMenuItem()
						
					menuobj['gtkSubMenu'].add(childobj['gtkMenuItem'])
					if childobj['callback'] != None:
						childobj['gtkMenuItem'].connect('activate', childobj['callback'])
					if 'accelkey' in childobj:
						if 'accelmods' not in childobj:
							childobj['accelmods'] = Gdk.ModifierType.CONTROL_MASK
						childobj['gtkMenuItem'].add_accelerator('activate',
							self.accelgroup,
							Gdk.keyval_from_name(childobj['accelkey']),
							childobj['accelmods'],
							Gtk.AccelFlags.VISIBLE)
	
		for obj in self.topmenus.values():
			self.mbr.add(obj['gtkMenuItem'])

		self.vbox.add(self.mbr)
		
		self.tbr = Gtk.Toolbar()
		
		# TODO button names and tooltips
		self.tbtn_open = Gtk.ToolButton()
		self.tbtn_open.set_icon_name('document-open')
		self.tbtn_open.connect('clicked', self.on_open_input)
		self.tbr.insert(self.tbtn_open, -1)
		
		self.tbtn_open = Gtk.ToolButton()
		self.tbtn_open.set_icon_name('document-save')
		self.tbtn_open.connect('clicked', self.on_save_input)
		self.tbr.insert(self.tbtn_open, -1)
		
		self.hbox_tbr_lang = Gtk.HBox()

		self.cbo_inlang = Gtk.ComboBoxText()
		self.cbo_outlang = Gtk.ComboBoxText()
		for lang in NAN_LANG_TITLES:
			self.cbo_inlang.append(lang, NAN_LANG_TITLES[lang])
			self.cbo_outlang.append(lang, NAN_LANG_TITLES[lang])

		self.cbo_inlang.set_active_id('en')
		self.cbo_outlang.set_active_id('ml')

		self.lbl_tbr_inlang = Gtk.Label(label=_('Input language: '))
		self.hbox_tbr_lang.add(self.lbl_tbr_inlang)
		self.hbox_tbr_lang.add(self.cbo_inlang)
		
		self.lbl_tbr_outlang = Gtk.Label(label=_('Output language: '))
		self.hbox_tbr_lang.add(self.lbl_tbr_outlang)
		self.hbox_tbr_lang.add(self.cbo_outlang)

		self.cbo_inlang.connect('changed', self.on_cbo_inlang_changed)		
		self.cbo_outlang.connect('changed', self.on_cbo_outlang_changed)

		self.toolitem_outlang = Gtk.ToolItem()
		self.toolitem_outlang.add(self.hbox_tbr_lang)
		self.tbr.insert(self.toolitem_outlang, -1)
		
		self.vbox.add(self.tbr)
		self.vbox.set_child_packing(self.tbr, False, False, 0, 0)
		
		self.notebook = Gtk.Notebook()
		self.notebook.set_action_widget(Gtk.Button(label='+'), Gtk.PackType.END) # TODO FIXME
		
		self.vbox.add(self.notebook)
		
		self.wnd.add(self.vbox)
		self.vbox.set_child_packing(self.mbr, False, False, 0, 0)
		
		self.on_mnu_tab_new()
		self.tabs[-1].nanEditorPage.txtview_in.grab_focus()

		try:
			self.back = NanParayumpoleBackend() # TODO FIXME XXX Can only be done at last since Gtk errors would interrupt otherwise?
		except Exception as e:
			print(e) # TODO stderr
			self.nan_error_dialog(_("Could not start the Parayumpole backend."), _("Error"))
			sys.exit(1)
	
	def cur_tab_convert(self):
		txtbuf = self.notebook.get_nth_page(self.notebook.get_current_page()).txtbuf_in
		self.on_txtbuf_in_changed(txtbuf)
	
	def nan_error_dialog(self, msg, title):
		dlg = Gtk.MessageDialog(self.wnd,
			Gtk.DialogFlags.MODAL | Gtk.DialogFlags.DESTROY_WITH_PARENT,
			Gtk.MessageType.ERROR,
			Gtk.ButtonsType.OK,
			msg)
		dlg.set_title(title)
		dlg.run()
		dlg.destroy()
		
	def on_about(self, widget = None, event = None):
		# TODO FIXME do not hardcode strings
		dia = Gtk.AboutDialog()
		dia.set_transient_for(self.wnd)
		dia.set_title("About Parayumpole")
		dia.set_program_name("Parayumpole")
		dia.set_version(VERSION)
		dia.set_logo_icon_name("parayumpole")
		dia.set_comments('Bi-directional transliteration tool for Indian languages')
		dia.set_copyright("Copyright (C) 2012, 2013, 2017, 2018, 2022 Nandakumar Edamana <nandakumar@nandakumar.co.in>");
		dia.set_authors(["Nandakumar Edamana <nandakumar@nandakumar.co.in>"])
		
		dia.run()
		dia.destroy()

	def on_quit(self, widget = None, event = None):
		for tab in self.tabs:
			if tab.changed == True and tab.saved == False:
				dlg_confirm = Gtk.MessageDialog( self.wnd,
					Gtk.DialogFlags.MODAL | Gtk.DialogFlags.DESTROY_WITH_PARENT,
					Gtk.MessageType.QUESTION,
					Gtk.ButtonsType.YES_NO,
					_('You have inputs with unsaved changes. Are you sure you want to quit?') )
				resp = dlg_confirm.run()
				dlg_confirm.destroy()
				if resp == Gtk.ResponseType.YES:
					break
				else:
					self.quit_confirm_shown = True
					return True # Do not invoke other handlers
		self.back.__del__() # TODO FIXME not called automatically?
		sys.exit(0)


	def on_cbo_inlang_changed(self, widget = None):
		self.back.set_inlang(widget.get_active_id())
		self.cur_tab_convert()
	
	def on_cbo_outlang_changed(self, widget = None):
		self.back.set_outlang(widget.get_active_id())
		self.cur_tab_convert()

	def on_destroy(self, widget = None):
		if self.quit_confirm_shown == False:
			self.on_quit() # TODO FIXME how can this call cancel the destruction if the next statement is sys.exit(0)? Don't I have to evaluate the retval of self.on_quit() ?
		sys.exit(0)
	
	def on_mnu_tab_new(self, widget = None):
		# TODO add to global array, keep track of number, set label, etc.
		self.tabs.append(NanTab());
		self.tabcount_untitled += 1
		self.tabs[-1].title = 'Tab ' + str(self.tabcount_untitled)
		
		page = NanEditorPage(self, self.tabs[-1])
		self.notebook.append_page(page.vbox, Gtk.Label(label=self.tabs[-1].title));
		page.vbox.show_all()
		self.notebook.set_current_page(-1)
	
	def on_online_help(self, widget = None):
		webbrowser.open(ONLINE_HELP_PAGE);
	
	def on_open_input(self, widget = None):
		dlg = Gtk.FileChooserDialog( _("Open Input"),
			self.wnd,
			Gtk.FileChooserAction.OPEN,
			buttons = (Gtk.STOCK_OPEN, Gtk.ResponseType.ACCEPT, Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL) )
		resp = dlg.run()
		if resp == Gtk.ResponseType.ACCEPT:
			fname = dlg.get_filename()
			if os.path.isfile(fname):
				self.on_mnu_tab_new()
				self.tabs[-1].nanEditorPage.txtbuf_in.set_text(open(fname, 'r').read())
				self.tabs[-1].fname = fname
				self.tabs[-1].set_saved()
			else:
				self.nan_error_dialog(_('Error opening file') + " '" + fname + "'", _('Error'))
			
		dlg.destroy()
	
	def on_save_common(self, txtbuf):
		should_i_save = True;
		
		while True:
			dlg = Gtk.FileChooserDialog( _('Save Input'),
				self.wnd,
				Gtk.FileChooserAction.SAVE,
				buttons = (_('_Save'), Gtk.ResponseType.ACCEPT, _('_Cancel'), Gtk.ResponseType.CANCEL) )
			resp = dlg.run()
			if(resp != Gtk.ResponseType.ACCEPT):
				dlg.destroy()
				break;
				
			filename = dlg.get_filename()
			if(os.path.isfile(filename)):
				dlg_confirm = Gtk.MessageDialog( self.wnd,
					Gtk.DialogFlags.MODAL | Gtk.DialogFlags.DESTROY_WITH_PARENT,
					Gtk.MessageType.QUESTION,
					Gtk.ButtonsType.YES_NO,
					_('File already exists. Overwrite?') )
				resp2 = dlg_confirm.run()
				dlg_confirm.destroy()
				
				if(resp2 == Gtk.ResponseType.NO):
					should_i_save = False
				else:
					should_i_save = True
					dlg.destroy()
			else:
				should_i_save = True
				dlg.destroy()

			if should_i_save:
				text = txtbuf.get_text(txtbuf.get_start_iter(), txtbuf.get_end_iter(), False) # last param: include_hidden_chars
				open(filename, 'w').write(text)
				if 'nanTab' in txtbuf.__dict__: # Only txtbuf_in will have this
					txtbuf.nanTab.set_saved()
					txtbuf.nanTab.fname = filename
				break
		
	def on_save_input(self, widget = None):
		txtbuf = self.notebook.get_nth_page(self.notebook.get_current_page()).txtbuf_in # TODO common fun?
		if txtbuf.nanTab.fname:
			text = txtbuf.get_text(txtbuf.get_start_iter(), txtbuf.get_end_iter(), False) # last param: include_hidden_chars
			open(txtbuf.nanTab.fname, 'w').write(text)
			txtbuf.nanTab.set_saved()
		else:
			self.on_save_common(txtbuf)
		
	def on_save_output(self, widget = None):
		txtbuf = self.notebook.get_nth_page(self.notebook.get_current_page()).txtbuf_out
		self.on_save_common(txtbuf)
		
	def on_txtbuf_in_changed(self, widget = None): # widget should be a txtbuf_in
		inp = widget.get_text(widget.get_start_iter(), widget.get_end_iter(), False) # last param: include_hidden_chars
		conv = self.back.convert(inp)
		if conv == None:
			self.nan_error_dialog(_("Couldn't communicate with the backend. Please save your work and restart Parayumpole."), _('Error'))
			return
		widget.txtbuf_out.set_text(conv)
		widget.nanTab.set_changed()

if __name__  ==  '__main__':
	main()
	Gtk.main()
