/*    This file is part of the desktop version of
 *    Parayumpole bi-directional transliteration tool for Indian languages v0.4.2
 *    
 *    backend.js
 *    
 *    Copyright (C) 2018 Nandakumar Edamana <nandakumar@nandakumar.co.in>
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/* 20 Apr 2018: @myself: Read my personal hard-copy diary to see how I reached this code.
 * Some (really passive) help from: https://devdocs.baznga.org/, GIO Reference Manual (for C)
 */

const libparayumpole = imports.libparayumpole;

const gio = imports.gi.Gio;
const uis = new gio.UnixInputStream({fd: 0}); // stdin
const ues = new gio.UnixOutputStream({fd: 2}); // stderr TODO REM ?
const uos = new gio.UnixOutputStream({fd: 1}); // stdout
const dis = new gio.DataInputStream({base_stream: uis}); // stdin

var NAN_DEBUG_MODE = false; // XXX

var out;

while(1) { // TODO REM debug messages?
	if(NAN_DEBUG_MODE)
		ues.write('Backend waiting...\n', null);
	var inlang = dis.read_line(null) [0] // param null: cancellable; [0] to extract the data field from the returned ByteArray [the other field is len, and this will be prepended to the text if the returned value is used directly (like "text,4")]
	if(NAN_DEBUG_MODE)
		ues.write('Backend read inlang: ' + inlang + '\n', null);
			
	var outlang = dis.read_line(null) [0]
	if(NAN_DEBUG_MODE)
		ues.write('Backend read outlang: ' + outlang + '\n', null);
		
	var text = dis.read_line_utf8(null) [0]
	if(NAN_DEBUG_MODE)
		ues.write('Backend read text: ' + text + '\n', null);
		
	if(inlang == 'en') {
		out = libparayumpole.nan_transliterate(text, outlang);
	}
	else if(outlang == 'en') {
		out = libparayumpole.nan_transliterate_reverse(text, inlang);
	}
	else { /* One Indian language to another */
		var en = libparayumpole.nan_transliterate_reverse(text, inlang);
		out = libparayumpole.nan_transliterate(en, outlang);
	}
	
	uos.write(out + '\n', null)
	
	if(NAN_DEBUG_MODE)		
		ues.write('Backend out: ' + out + '\n', null);
}
