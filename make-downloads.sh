#!/bin/sh
# Nandakumar Edamana
# 21 Apr 2018

origdir="$(pwd)"
make dist
rm -rf /tmp/parayumpole*
mv parayumpole-*.tar.gz /tmp
cd /tmp
tar -xvf parayumpole-*.tar.gz
cd parayumpole-*
dpkg-buildpackage
cd ..
lintian parayumpole*.deb
cd "$origdir"
mkdir downloads-tmp
cp /tmp/parayumpole-*.tar.gz /tmp/parayumpole*.deb downloads-tmp
