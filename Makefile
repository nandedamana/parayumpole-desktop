DISTFILES = Makefile README TODO debian icons man parayumpole.desktop src
DISTVER = 0.4.2
DISTNAME = parayumpole-${DISTVER}
DEBNAME = parayumpole_${DISTVER}

all:

dist: ${DISTNAME}.tar.gz

${DISTNAME}.tar.gz:
	@if [ ! -d ${DISTNAME} ]; then\
		mkdir ${DISTNAME};\
	fi
	cp -r ${DISTFILES} ${DISTNAME}/
	tar -czf ${DISTNAME}.tar.gz ${DISTNAME}

deb: ${DEBNAME}.deb

${DEBNAME}.deb: ${DISTNAME}.tar.gz
	cd ${DISTNAME} && dpkg-buildpackage && lintian && lintian ${DEBNAME}_*.changes

install:
	mkdir ${DESTDIR}/usr/bin -p
	mkdir ${DESTDIR}/usr/lib/parayumpole -p
	mkdir ${DESTDIR}/usr/share/applications -p
	mkdir ${DESTDIR}/usr/share/icons -p
	mkdir ${DESTDIR}/usr/share/man/man1 -p
	cp src/parayumpole.py ${DESTDIR}/usr/bin/parayumpole
	cp src/libparayumpole.js ${DESTDIR}/usr/lib/parayumpole/
	cp src/backend.js ${DESTDIR}/usr/lib/parayumpole/
	cp parayumpole.desktop ${DESTDIR}/usr/share/applications/
	cp icons/parayumpole.svg ${DESTDIR}/usr/share/icons/parayumpole.svg
	cp icons/parayumpole_192.png ${DESTDIR}/usr/share/icons/parayumpole.png
	cp man/parayumpole.1 ${DESTDIR}/usr/share/man/man1
	gzip ${DESTDIR}/usr/share/man/man1/parayumpole.1

clean:
	rm -rf ${DEBNAME}.* ${DEBNAME}_* ${DISTNAME} ${DISTNAME}.tar.gz
